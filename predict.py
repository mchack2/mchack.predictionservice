import re 
import json 
import numpy as np 
import pandas as pd 
import joblib 
from sentence_transformers import SentenceTransformer 
from tqdm import tqdm 
from sklearn.metrics.pairwise import cosine_similarity 
from nltk.corpus import stopwords
import pymorphy2  
import sys
from flask import Flask, jsonify, request

app = Flask(__name__)
morph = pymorphy2.MorphAnalyzer()
stops = set(stopwords.words("english")) | set(stopwords.words("russian"))  

def review_to_wordlist(review):  
    #1) Сначала функция будет удалять все символы кроме букв верхнего и нижнего регистра;  
    review_text = re.sub("[^а-яА-Яa-zA-Z]"," ", review)  
    #2) Затем преобразовывает слова к нижнему регистру;  
    words = review_text.lower().split()  
    #3) После чего удаляет стоп слова из текста, т.к. они не несут никакой информации о содержании;  
    words = [w for w in words if not w in stops]  
    #4) Лемматизация, процесс приведения словоформы к лемме — её нормальной (словарной) форме.  
    words = [morph.parse(w)[0].normal_form for w in words ]  
    return(words) 
 
@app.route("/mchack/predict", methods=['POST'])
def predict(): 
    input = request.get_json()['input']
    words = review_to_wordlist(input) 
    normStr = ' '.join(words) 
 
    model = SentenceTransformer('distiluse-base-multilingual-cased-v1') 
    strVec = model.encode(normStr, batch_size = 8) 
     
    clf = joblib.load("./random_forest.joblib") 
    ans = clf.predict([strVec]) 

    return jsonify({"result": int(ans[0])})
 

#answer = predict("станок п-1000")
#print(answer)

if __name__ == "__main__":
    from gevent import pywsgi
    server = pywsgi.WSGIServer(('0.0.0.0', 1235), app, log=sys.stdout)
    server.serve_forever()
